/********************* (C) COPYRIGHT 2009 e-Design Co.,Ltd. *********************
 File name  : File.h
 File description : the header file 
 Author    : bure  
 Hardware: DS0201V1.1 
 Version: Ver 1.0.1         
 Finished  :2009/08/06   
 *********************************************************************************/
#ifndef __FILE_H
#define __FILE_H
//================================= Function declarations ===================================
char            FAT_Info(void);
char            Open_BMP_File(unsigned char *n);
char            Open_DAT_File(unsigned char *n);
char            Writ_BMP_File(void);

//char Open_CFG_File(void);
char            Read_File(void);
char            Write_File(void);
void            Read_Parameter(void);
char            Write_Parameter(void);

#endif
/********************************* END OF FILE ********************************/
