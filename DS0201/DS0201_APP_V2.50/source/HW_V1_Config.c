/******************** (C) COPYRIGHT 2009 e-Design Co.,Ltd. *********************
File Name: HW_V1_config.c 
Author: bure   
Hardware: DS0201V1.1~1.6 
Version: V1.0
Translated by Deyou, Wang, Seeed Studio, Sep 16th 
*******************************************************************************/
#include "Function.h"
#include "stm32f10x_it.h"
#include "stm32f10x_systick.h"
#include "HW_V1_Config.h"
unsigned char   Key_Status_Last = 0;

ErrorStatus     HSEStartUpStatus;

/*******************************************************************************
Function Name : GPIO_Config
Description : gpio configuration 
Para : NONE
Return:	 NONE 
*******************************************************************************/
void        GPIO_Config(void)
{
   GPIOA_CRL = 0x88888080; /*
                            * GPIOA Bit0-7 status setting 
							* |||||||+----- Nib0  Ain   analog input
                            * ||||||+------ Nib1  NC    pullup input
                            * |||||+------- Nib2  Vbat  analog input 
                            * ||||+-------- Nib3  KB    pullup input  
                            * |||+--------- Nib4  KP    pullup input  
                            * ||+---------- Nib5  KL    pullup input  
                            * |+----------- Nib6  KU    pullup input  
                            * +------------ Nib7  KR    pullup input
                            */
   GPIOA_CRH = 0x8884488B; /*
                            * GPIOA Bit8-15 status setting
							* |||||||+----- Nib8  MCO    multiplexer output
                            * ||||||+------ Nib9  NC     pullup input
                            * |||||+------- Nib10 NC     pullup input  
                            * ||||+-------- Nib11 D-    Hi-impedance input  
                            * |||+--------- Nib12 D+    Hi-impedance input  
                            * ||+---------- Nib13 TMS    pullup input  
                            * |+----------- Nib14 TCK    pullup input  
                            * +------------ Nib15 TDI    pullup input
                            */
   GPIOA_ODR = 0x0FFFF;

   GPIOB_CRL = 0x88888411; /*
                            * GPIOB Bit0-7 status setting
							* |||||||+----- Nib0  A     lower speed output
                            * ||||||+------ Nib1  D     lower speed output
                            * |||||+------- Nib2  BT1   Hi-impedance input  
                            * ||||+-------- Nib3  TDO   pullup input  
                            * |||+--------- Nib4  TRST  pullup input  
                            * ||+---------- Nib5  NC    pullup input  
                            * |+----------- Nib6  NC    pullup input  
                            * +------------ Nib7  NC    pullup input
                            */
   GPIOB_CRH = 0xBBB1B488; /*
                            * GPIOB Bit8-15 status setting 
							* |||||||+----- Nib8  NC    pullup input
                            * ||||||+------ Nib9  NC    pullup input
                            * |||||+------- Nib10 Vusb  Hi-impedance input  
                            * ||||+-------- Nib11 V0    multiplexer output  
                            * |||+--------- Nib12 SDCS  lower speed output  
                            * ||+---------- Nib13 SCK   multiplexer output  
                            * |+----------- Nib14 MISO  multiplexer output  
                            * +------------ Nib15 MOSI  multiplexer output
                            */
   GPIOB_ODR = 0x0FFFF;

   GPIOC_CRL = 0x84118881; /*
                            * GPIOC Bit0-7 status setting 
							* |||||||+----- Nib0  nCLS  lower speed output
                            * ||||||+------ Nib1  NC    pullup input
                            * |||||+------- Nib2  NC    pullup input  
                            * ||||+-------- Nib3  NC    pullup input  
                            * |||+--------- Nib4  C     lower speed output  
                            * ||+---------- Nib5  B     lower speed output  
                            * |+----------- Nib6  NC    Hi-impedance input  
                            * +------------ Nib7  NC    pullup input
                            */
   GPIOC_CRH = 0x88888884; /*
                            * GPIOC Bit8-15 status setting 
							* |||||||+----- Nib8  NC    Hi-impedance input
                            * ||||||+------ Nib9  NC    pullup input
                            * |||||+------- Nib10 NC    pullup input  
                            * ||||+-------- Nib11 NC    pullup input  
                            * |||+--------- Nib12 NC    pullup input  
                            * ||+---------- Nib13 NC    pullup input  
                            * |+----------- Nib14 NC    pullup input  
                            * +------------ Nib15 NC    pullup input
                            */
   GPIOC_ODR = 0x0FFFF;

   GPIOD_CRL = 0x38338838; /*
                            * GPIOD Bit0-7 status setting 
							* |||||||+----- Nib0  NC    pullup input
                            * ||||||+------ Nib1  RS    high speed output
                            * |||||+------- Nib2  NC    pullup input  
                            * ||||+-------- Nib3  NC    pullup input  
                            * |||+--------- Nib4  nRD   high speed output  
                            * ||+---------- Nib5  nWR   high speed output  
                            * |+----------- Nib6  NC    pullup input  
                            * +------------ Nib7  nCS   high speed output
                            */
   GPIOD_CRH = 0x88838884; /*
                            * GPIOD Bit8-15 status setting 
							* |||||||+----- Nib8  NC    pullup input 
                            * ||||||+------ Nib9  KD    pullup input 
                            * |||||+------- Nib10 SDDT  pullup input   
                            * ||||+-------- Nib11 KM    pullup input   
                            * |||+--------- Nib12 Fout  high speed& multiplexer output
                            * ||+---------- Nib13 NC    pullup input   
                            * |+----------- Nib14 NC    pullup input   
                            * +------------ Nib15 NC    pullup input 
                            */
   GPIOD_ODR = 0x0FF7F;

   GPIOE_CRL = 0x33333333; // Bit0-7  high speed output��LCD_DB0-7 ��

   GPIOE_CRH = 0x33333333; // Bit8-15 high speed output��LCD_DB8-15��

   AFIO_MAPR = 0x00001200; /*
                            * AFIO remapping  the register 
							*/
}

/*******************************************************************************
Function Name : DMA_configuration
Description : initialize the DMA channel one,ues it to save and transfer the ADC sampling data
Para : NONE
Return:	 NONE 
*******************************************************************************/
void     DMA_Configuration(void)
{
   DMA_ISR = 0x00000000; /*
                          * DMA Interrupt Status Register��through setting IFCR corresponding bits to clear
                          * |||||||+---GIF1 =0��channel one don't produce TE,HT or TC event global interrupt mark
                          * |||||||+---TCIF1=0��channel one don't produce transmission end interrupt mark
                          * |||||||+---HTIF1=0��channel one don't produce half transmission end interrupt mark
                          * |||||||+---TEIF1=0��channel one don't produce transmission error interrupt mark
                          * ||||||+----channel two interrupt flags
                          * ||||++-----channel three interrupt flags
                          * ||||++-----channel four interrupt flags
                          * |||+-------channel five interrupt flags
                          * ||+--------channel six interrupt flags 
                          * ||+--------channel seven interrupt flags  +----------reserved
                          */
   DMA_IFCR = 0x00000000; /*
                           * DMA cleared interrupt flag register that can be setted or cleared by software
                           * |||||||+---CGIF1=1��clear the channel one  TE,HT or TC event global interrupt mark
                           * |||||||+---CTCIF1=1��clear the chanel one transmission end interrupt mark
                           * |||||||+---CHTIF1=1��clear the chanel one half transmission end interrupt mark
                           * |||||||+---CTEIF1=1��clear the chanel one transmission error interrupt mark
                           * ||||||+----channel two interrupt flags
                           * ||||++-----channel three interrupt flags
                           * ||||++-----channel four interrupt flags
                           * |||+-------channel five interrupt flags
                           * ||+--------channel six interrupt flags 
                           * ||+--------channel seven interrupt flags  +----------reserved
                           */
   DMA_CCR1 = 0x00003580; /*
                           * 0011 0101 1000 0000   ||||       |||| |||| ||||
                           * |||+--EN = 0��close channel��1��open the channel ||||
                           * |||| |||| |||| ||+---TCIE=0��disable the transmition end interrupt
                           * ||||       |||| |||| ||||
                           * |+----HTIE=0��disable half transmition  interrupt ||||       ||||
                           * |||| |||| +-----TEIE=0��disable transmition error interrupt ||||
                           *    |||| |||| |||+-------DIR
                           * =0��Data transmission directions��from peripheral �� ||||       ||||
                           * |||| ||+--------CIRC=0��do not execute loop operation||||
                           *  |||| ||||
                           * |+---------PINC=0��do not execute increment mode of the peripheral's address  ||||
                           *      |||| ||||
                           * +----------MINC=1��execute increment mode of the memorizer's address ||||
                           *     ||||
                           * ||++------------PSIZE=01��16 bit data bandwidth for peripheral ||||
                           *      ||||
                           * ++--------------MSIZE=01��16 bit data bandwidth for memorizer
                           * ||||
                           * ||++-----------------PL=11��highest priorty of the channel  ||||
                           * 
                           * |+-------------------MEM2MEM=0��from non memorizer mode to memorizer mode
                           * ++++-------+--------------------reserved
                           */

   DMA_CNDTR1 = 0x00001000; // transmition data register of the DMA1 (1024*2 Bytes)  

   DMA_CPAR1 = ADC1_DR_ADDR; // base address of the peripheral's  data register for DMA1

   DMA_CMAR1 = (u32) & Scan_Buffer[0]; // storage address for DMA1

   DMA_CCR1 |= 0x00000001; // EN = 1��open the DMA channel one

}
/*******************************************************************************
Function Name : ADC_Configuration
Description : initialize the ADC status,transfer the data to buffer by DMA after sampling
Para : NONE
Return:	 NONE 
*******************************************************************************/
void       ADC_Configuration(void)
{
   ADC2_CR1 = 0x00000000;
   ADC1_CR1 = 0x00000000; /*
                           * ||||||++---AWDCH[4:0]��channel selection bits for analog watchdog
                           * ||||||+----=000��disable EOC��AWD��JEOC interrupt
                           * |||||+-----Nib8_SCAN=0��do not use scan mode
                           * ||||++-----remain the reset value
                           * |||+-------DUALMOD=0��independent mode
                           * ||+--------disable analog watchdog in the regulation and injection channel
                           * ++---------reserved
                           */
   ADC2_CR2 = 0x00100000;
   ADC1_CR2 = 0x00100100; /*
                           * |||||||+---ADON=0��close ADC transition/calibration��=1��enable ADC and start transition
                           * |||||||+---CONT=0��1��continuous transition mode 
                           * |||||||+---CAL=0��=1��after finished the AD calibration,clear the corresponding bit by hardware 
                           * |||||||+---RSTCAL=0��=1��init the calibration register and cleared it when finishend
                           * ||||||+----reserved |||||+-----ALIGN=0 &
                           * DMA=1��align right and DMA mode ||||+------
                           * |||+-------=0000��regulation channel ues the CC1 event of Timer1 to start transition��=1110��SWSTART
                           * ||+--------=1��use the external trigger to start transition   ++---------reserved
                           */
   ADC2_SQR1 = 0x00000000;
   ADC1_SQR1 = 0x00000000; /*
                            * ADC rule sequence register one
                            * ||++++++---SQ13-16=00000��the 13-16 conversion channel number in the conversion array(5Bits*4)
                            * ++---------reversed
                            */
   ADC2_SQR3 = 0x00000002;
   ADC1_SQR3 = 0x00000000; /*
                            * ADC rule sequence register three
                            * ||||||++---SQ1=00000��the first conversion channel number in the conversion array(0)
                            * +++++++----SQ2-6=00000��the 2-6 conversion channel number in the conversion array(5Bits*5)
                            * +----------reserved
                            */
   ADC2_SMPR2 = 0x00000000;
   ADC1_SMPR2 = 0x00000000; /*
                             * ADC sampling timer registers one  
                             * |||||||+---SMP01=001(3Bits)��the sampling time of the channel 00  
                             * 1.5T   
                             * ||||+------SMP04=001(3Bits)��the sampling time of the channel 04
                             * 7.5T
                             * |+++++++---SMP11-17=000(3Bits*7)��the sampling time of the channel 11-17
                             * 1.5T ++---------reserved
                             */
   ADC1_CR2 |= 0x00000001;
   ADC2_CR2 |= 0x00000001; // ADON=1��start ADC1��ADC2

   ADC1_CR2 |= 0x00000008;
   while (ADC1_CR2 & 0x00000008); // init the calibration register of the ADC1

   ADC2_CR2 |= 0x00000008;
   while (ADC2_CR2 & 0x00000008); //  init the calibration register of the ADC1

   ADC1_CR2 |= 0x00000004;
   while (ADC1_CR2 & 0x00000004); // ADC1 calibration

   ADC2_CR2 |= 0x00000004;
   while (ADC2_CR2 & 0x00000004); // ADC2 calibration

}
/*******************************************************************************
Function Name : Timer_Configuration
Description : initialize the system timer
Para : NONE
Return:	 NONE 
*******************************************************************************/
void      Timer_Configuration(void)
{
//---------------TIM1 for trigger ADC sample at regular time -------------------
   Set_Base(Item_Index[2]);
   TIM1_CR1 = 0x0094; /*
                       * 0000 0000 1001 0100   |||| |||| ||||
                       * |||+---CEN=0��disable timer |||| |||| ||||
                       * ||+----UDIS=0��allow UEV update |||| |||| ||||
                       * |+-----URS=1��produce interrupt or DMA requst only when the counter overflow   ||||
                       * |||| ||||
                       * +------OPM=0��When the updating  event occurs , counter doesn't stop  ||||
                       * |||| |||+--------DIR=1��down counter |||| ||||
                       * |++---------CMS=00��Edge alignment mode |||| ||||
                       * +-----------ARPE=1��put TIM1_ARR to buffer  ||||
                       * ||++-------------CKD=00,CK_INT frequency dividing ratio is 1
                       * ++++-++---------------reserved
                       */
   TIM1_RCR = 0x0000; /*
                       * 0000 0000 0000 0001   |||| |||| ++++
                       * ++++---the PWM cycle add one in the edge alignment mode 
                       * ++++-++++-------------reserved
                       */
   TIM1_CCER = 0x0001; /*
                        * 0000 0000 0000 0001   |||| |||| ||||
                        * |||+---CC1E=1��OC1 signal output to the coresponding pin ||||
                        * |||| |||| ||+----CC1P=0��OC1 High Level On |||| ||||
                        * |||| |+-----CC1NE=0��OC1N disable output |||| |||| ||||
                        * +------CC1NP=0��OC1N High Level On
                        * ++++-++++-++++--------CC2��CC3��CC4 setted to the reset value
                        */
   TIM1_CCMR1 = 0x0078; /*
                         * 0000 0000 0111 1100   |||| |||| ||||
                         * ||++---CC1S=00��CC1 channel setted fot output |||| |||| |||| 
                         * |+-----OC1FE=1�� |||| ||||
                         * |||| +------OC1PE=1��  ||||
                         * |||| |+++--------0C1M=111��PWM mode2 |||| ||||
                         * +-----------OC1CE=0��ETRF input  do not affet OC1REF 
                         * ++++-++++-------------CC2 channel for reset
                         */
   TIM1_BDTR = 0x8000; /*
                        * 1000 0000 0000 0000  
                        * |+++-++++-++++-++++---other bits remain unchanged 
                        * +---------------------MOE=0��enable the main output
                        */
   TIM1_DIER = 0x4200; /*
                        * 0100 0011 0000 0000  DMA and Interrupt enabled registers  |     ||
                        *      +----CC1IE=0��disable capture/compare interrupt one |
                        * |+-------------UDE=1��allow the request of the DMA update  |
                        * +--------------CC1DE=1��allow capture/compare DMA1 request
                        * +--------------------TDE=1��enable request to trigger DMA 
                        */
   TIM1_CR1 |= 0x0001; // CEN=1��enable TIMER1 counter 

//-----------------TIM2_CH4 used for PWM output the offset voltage of Y channel -------------------   
   TIM2_PSC = 0;
   TIM2_ARR = 3839; // 72MHz/3840=18.75KHz 

   Set_Y_Pos(Item_Index[Y_SENSITIVITY], Item_Index[V0]);
   TIM2_CR1 = 0x0084; /*
                       * 0000 0000 1000 0100   |||| |||| ||||
                       * |||+---CEN=0��disable counter |||| |||| ||||
                       * ||+----UDIS=0��allow UEV update |||| |||| ||||
                       * |+-----URS=1��produce interrupt or DMA requst only when the counter overflow  ||||
                       * |||| ||||
                       * +------OPM=0��When the updating  event occurs , counter doesn't stop   ||||
                       * |||| |||+--------DIR=0��up counter |||| ||||
                       * |++---------CMS=00��Edge alignment mode |||| ||||
                       * +-----------ARPE=1��put TIM2_ARR to buffer  ||||
                       * ||++-------------CKD=00,CK_INT frequency dividing ratio is 1
                       * ++++-++---------------reserved
                       */
   TIM2_CCER = 0x3000; /*
                        * 0011 0000 0000 0000   ||||
                        * ++++-++++-++++---CC1��CC2��CC3 setted to the reset value
                        * |||+------------------CC4E=1��OC4 signal output to the coresponding pin
                        * ||+-------------------CC4P=1��OC4 Low Level On 
                        * |+--------------------CC4NE=0��OC4N disable output
                        * +---------------------CC4NP=0��OC4N High Level On
                        */
   TIM2_CCMR2 = 0x7C00; /*
                         * 0111 1100 0000 0000   |||| ||||
                         * ++++-++++---CC3 channel setted fot output ||||
                         * ||++-------------CC4S=00��CC4 used for output |||| 
                         * |+---------------OC4FE=1��
                         * |||| +----------------OC4PE=1��
                         * 
                         * |+++------------------0C4M=111��PWM mode2
                         * +---------------------OC4CE=0��ETRF input  do not affet OC4REF
                         * 
                         */
   TIM2_CR1 |= 0x0001; // CEN=1��enable TIMER2 counter 

//-----------------TIM3 for system timer-------------------  
   TIM3_PSC = 9;
   TIM3_ARR = 7199; // 1mS clock cycle =��PSC+1������ARR+1��/��72MHz/2����uS��

   TIM3_CCR1 = 3600; // pulse width��duty cycle��50%

   TIM3_CR1 = 0x0084; /*
                       * 0000 0000 1000 0100   |||| |||| ||||
                       * |||+---CEN=0��disable counter |||| |||| ||||
                       * ||+----UDIS=0��allow UEV update |||| |||| ||||
                       * |+-----URS=1��produce interrupt or DMA requst only when the counter overflow   ||||
                       * |||| ||||
                       * +------OPM=0��When the updating  event occurs , counter doesn't stop   ||||
                       * |||| |||+--------DIR=0��up counter |||| ||||
                       * |++---------CMS=00��Edge alignment mode |||| ||||
                       * +-----------ARPE=1��put  TIM3_ARR to buffer ||||
                       * ||++-------------CKD=00,CK_INT frequency dividing ratio is 1
                       * ++++-++---------------reserved
                       */
   TIM3_DIER = 0x0002; /*
                        * 0000 0000 0000 0010  DMA and Interrupt enabled registers |
                        * +----CC1IE=1��enable capture/compare interrupt one
                        * +--------------CC1DE=1��allow capture/compare DMA1 request
                        */
   TIM3_CR1 |= 0x0001; // CEN=1��enable TIMER3 

//------------------TIM4_CH1 for outputting the selected frequency rectangle wave signal----------------------
   TIM4_PSC = 7;
   TIM4_ARR = Fout_ARR[Item_Index[11]]; // clock cycle

   TIM4_CCR1 = (Fout_ARR[Item_Index[11]] + 1) / 2; // pulse width0.5mS��duty cycle��50%��

   TIM4_CR1 = 0x0084; /*
                       * 0000 0000 1000 0100   |||| |||| ||||
                       * |||+---CEN=0��disable counter |||| |||| ||||
                       * ||+----UDIS=0��allow UEV update |||| |||| ||||
                       * |+-----URS=1��produce interrupt or DMA requst only when the counter overflow  ||||
                       * |||| ||||
                       * +------OPM=0��When the updating  event occurs , counter doesn't stop  ||||
                       * |||| |||+--------DIR=0��up counter  |||| ||||
                       * |++---------CMS=00��edge alignment mode |||| ||||
                       * +-----------ARPE=1��put TIM3_ARR to buffer ||||
                       * ||++-------------CKD=00,CK_INT frequency dividing ratio is 1 
                       * ++++-++---------------reserved
                       */
   TIM4_CCER = 0x0001; /*
                        * 0000 0000 0000 0001   |||| |||| ||||
                        * |||+---CC1E=1��OC1 signal output to the coresponding pin  ||||
                        * |||| |||| ||+----CC1P=0��OC1 High Level On |||| ||||
                        * |||| |+-----CC1NE=0��OC1N disable output |||| |||| ||||
                        * +------CC1NP=0��OC1N High Level On
                        * ++++-++++-++++--------CC2��CC3��CC4 setted to the reset value
                        */
   TIM4_CCMR1 = 0x0078; /*
                         * 0000 0000 0111 1100   |||| |||| ||||
                         * ||++---CC1S=00��CC1  channel setted fot output |||| |||| |||| 
                         * |+-----OC1FE=1�� |||| ||||
                         * |||| +------OC1PE=1��  ||||
                         * |||| |+++--------0C1M=111��PWM mode2 |||| ||||
                         * +-----------OC1CE=0��ETRF input  do not affet OC1REF
                         * ++++-++++-------------CC2 channel for reset
                         */
   TIM4_CR1 |= 0x0001; // CEN=1��enable TIMER4 

}
/*******************************************************************************
Function Name : KeyScan
Description : get the input keycode
Para : NONE
Return:	 KeyCode: the keycode 
*******************************************************************************/
char      KeyScan(void)
{
   char Key_Status_Now = 0, KeyCode = 0;

   if ((~GPIOA_IDR) & 0x0010)
      Key_Status_Now |= 0x01; // KEY_PLAY_ON (KEY_A)

   if ((~GPIOD_IDR) & 0x0800)
      Key_Status_Now |= 0x02; // KEY_MENU_ON (KEY_OK)

   if ((~GPIOA_IDR) & 0x0040)
      Key_Status_Now |= 0x04; // KEY_UP_ON

   if ((~GPIOD_IDR) & 0x0200)
      Key_Status_Now |= 0x08; // KEY_DOWN_ON

   if ((~GPIOA_IDR) & 0x0020)
      Key_Status_Now |= 0x10; // KEY_LEFT_ON

   if ((~GPIOA_IDR) & 0x0080)
      Key_Status_Now |= 0x20; // KEY_RIGHT_ON

   if ((~GPIOA_IDR) & 0x0008)
      Key_Status_Now |= 0x40; // KEY_B_ON

   if (Key_Status_Now & (~Key_Status_Last))
   {

      Key_Wait_Counter = 25; // do not start automatically repeat function during continuous 500mS 

      if (Key_Status_Now & 0x01)
         KeyCode = KEYCODE_PLAY; // KeyCode(Play/A) 

      if (Key_Status_Now & 0x02)
         KeyCode = KEYCODE_MANU; // KeyCode(Manu/OK) 

      if (Key_Status_Now & 0x04)
         KeyCode = KEYCODE_UP; // KeyCode(Up) 

      if (Key_Status_Now & 0x08)
         KeyCode = KEYCODE_DOWN; // KeyCode(Down) 

      if (Key_Status_Now & 0x10)
         KeyCode = KEYCODE_LEFT; // KeyCode(Left) 

      if (Key_Status_Now & 0x20)
         KeyCode = KEYCODE_RIGHT; // KeyCode(Right) 

   } 
   else
   {

      if (Key_Status_Now & Key_Status_Last)
      {
         if ((Key_Wait_Counter | Key_Repeat_Counter) == 0)
         {
            if (Key_Status_Now & 0x04)
               KeyCode = KEYCODE_UP; // KeyCode(Up) 

            if (Key_Status_Now & 0x08)
               KeyCode = KEYCODE_DOWN; // KeyCode(Down) 

            if (Key_Status_Now & 0x10)
               KeyCode = KEYCODE_LEFT; // KeyCode(Left) 

            if (Key_Status_Now & 0x20)
               KeyCode = KEYCODE_RIGHT; // KeyCode(Right)

            Key_Repeat_Counter = 3; // automatically repeat once every 60ms
			

         }
      }
	  else
         Key_Wait_Counter = 25; // do not start automatically repeat function during continuous 500mS 

   }
   Key_Status_Last = Key_Status_Now;
   return KeyCode;
}
/*******************************************************************************
Function Name : Battery_Detect
Description : detect the battery status
Para : NONE
Return:	 NONE
*******************************************************************************/
void     Battery_Detect(void)
{
   unsigned int    vb;

//  vb =(123*(*((vu16 *)(ADC1_DR_ADDR+2))))>>10;
   vb = (133 * (*((vu16 *) (ADC1_DR_ADDR + 2)))) >> 10; //use ADC1 to detect the battery status
   Battery = vb_Sum / 32;
//  Battery=vb; 
   vb_Sum = vb_Sum + vb - Battery;
   Update[5] = 1;
//  if(Battery>380)    //Ver2.01
   if (Battery > 390) // Ver2.02

      Item_Index[5] = 3;  //flag of the battery status icon
   else if (Battery > 350)
      Item_Index[5] = 2;
   else if (Battery > 320)
      Item_Index[5] = 1;
   else
      Item_Index[5] = 0;
}
/*******************************************************************************
Function Name : Set_Range
Description : set the range of the voltage 
Para : Range is voltage range
Return:	 NONE
*******************************************************************************/
void     Set_Range(char Range)
{

   switch (Range)
   {
   case 0: // 10mV ��1/Div   

   case 1: // 20mV ��1/Div   

   case 2: // 50mV ��1/Div   

   case 3: // 0.1V ��1/Div   

      RANGE_A_HIGH();
      RANGE_B_HIGH();
      RANGE_C_HIGH();
      RANGE_D_HIGH();
      break;
   case 4: // 0.2V ��1/Div    

   case 5: // 0.5V ��1/Div   

   case 6: // 1V ��1/Div   

      RANGE_A_HIGH();
      RANGE_B_HIGH();
      RANGE_C_HIGH();
      RANGE_D_LOW();
      break;
   case 7: // 2V ��1/Div    

   case 8: // 5V ��1/Div

   case 9: // 10V ��1/Div

      RANGE_A_LOW();
      RANGE_B_HIGH();
      RANGE_C_HIGH();
      RANGE_D_LOW();
      break;
   case 10: // 0.2V/Div ��20mV ��10/Div�� 

   case 11: // 0.5V/Div ��50mV ��10/Div��

   case 12: // 1V/Div ��0.1V ��10/Div��

      RANGE_A_HIGH();
      RANGE_B_LOW();
      RANGE_C_HIGH();
      RANGE_D_LOW();
      break;
   case 13: // 2V/Div ��0.2V ��10/Div��

   case 14: // 5V/Div ��0.5V ��10/Div��

   case 15: // 10V/Div ��  1V ��10/Div��

      RANGE_A_LOW();
      RANGE_B_LOW();
      RANGE_C_HIGH();
      RANGE_D_LOW();
      break;
   case 16: // 20V/Div �� 2V ��10/Div��

   case 17: // 50V/Div �� 5V ��10/Div��

   case 18: // 100V/Div ��10V ��10/Div��

      RANGE_A_HIGH();
      RANGE_B_HIGH();
      RANGE_C_LOW();
      RANGE_D_LOW();
      break;
   default: // Connecte To GND

      RANGE_A_LOW();
      RANGE_B_LOW();
      RANGE_C_LOW();
      RANGE_D_LOW();
      break;
   }
}

/*******************************************************************************
Function Name : Set_Base
Description : set the base level of the Horizontal scan
Para : Base is the index of the Scan_PSC&Scan_ARR
Return:	 NONE
*******************************************************************************/
void     Set_Base(char Base)
{
   TIM1_PSC = Scan_PSC[Base];
   TIM1_ARR = Scan_ARR[Base];
   TIM1_CCR1 = (Scan_ARR[Base] + 1) / 2;
}
/*******************************************************************************
Function Name : ADC_Start
Description : restart the ADC sampling scan
Para : NONE
Return:	 NONE
*******************************************************************************/
void     ADC_Start(void)
{
   DMA_CCR1 = 0x00000000;
   DMA_CMAR1 = (u32) & Scan_Buffer[0]; // reset the DMA channel 1

   DMA_CNDTR1 = 0x00001000;
   DMA_CCR1 |= 0x00000001; // restart the sample scan

}
/*******************************************************************************
Function Name : Set_Y_Pos
Description : set the baseline of the no-voltage
Para : i is the index of the array, Y0 is the voltage 
Return:	 NONE
*******************************************************************************/
void    Set_Y_Pos(unsigned short i, unsigned short Y0)
{
   TIM2_CCR4 = ((240 - Y0) * Y_POSm[i]) / 32 + Y_POSn[i];

}
/*******************************************************************************
Function Name : Test_USB_ON
Description : detect the USB status
Para : NONE
Return:	 1: USB plug in, 0:USB plug out
*******************************************************************************/
char            Test_USB_ON(void)
{
   if (GPIOB_IDR & 0x0400)
      return 1;
   else
      return 0;
}

/*******************************************************************************
Function Name : SD_Card_ON
Description : detect the SD card status
Para : NONE
Return:	 1: SD card plug in, 0:SD card plug out
*******************************************************************************/
char            SD_Card_ON(void)
{
   if (GPIOD_IDR & 0x0400)
      return 0;
   else
      return 1; // SD_Card_ON

}

/******************************** END OF FILE *********************************/
