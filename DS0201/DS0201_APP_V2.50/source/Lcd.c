/******************** (C) COPYRIGHT 2009 e-Design Co.,Ltd. *********************
File Name: lcd.c      
Author: bure      
Hardware: DS0201V1.1~1.6
Version: V1.0
Translated by Deyou, Wang, Seeed Studio, Sep 16th 
*******************************************************************************/
#include "Function.h"
#include "Lcd.h"
#include "stm32f10x_it.h"
#include "HW_V1_Config.h"
#include "ASM_Funtion.h"

/*******************************************************************************
Function Name : LCD_SET_WINDOW
Description : use  (x1,y1) and (x2,y2) to set a rectangle  area
Para :  (x1,y1) and (x2,y2) 
Return:	 NONE
*******************************************************************************/
void    LCD_SET_WINDOW(short x1, short x2, short y1, short y2)
{
   __LCD_WR_REG(0x0050, y1);
   __LCD_WR_REG(0x0051, y2);
   __LCD_WR_REG(0x0052, x1);
   __LCD_WR_REG(0x0053, x2);

   __LCD_WR_REG(0x0020, y1);
   __LCD_WR_REG(0x0021, x1);

   LDC_DATA_OUT = 0x0022; // Reg. Addr.

   LCD_RS_LOW(); // RS=0,Piont to Index Reg.

   LCD_nWR_ACT(); // WR Cycle from 1 -> 0 -> 1

   LCD_nWR_ACT(); // WR Cycle from 1 -> 0 -> 1

   LCD_RS_HIGH(); // RS=1,Piont to object Reg.

}
/*******************************************************************************
Function Name : Clear_Screen
Description : clear screen
Para :  Color is black
Return:	 NONE 
*******************************************************************************/
void    Clear_Screen(unsigned short Color)
{
   unsigned int    i;

   __Point_SCR(0, 0); // X_pos=0��Y_pos=0

   for (i = 0; i < 240 * 320; ++i)
      __Set_Pixel(Color);
}
/*******************************************************************************
Function Name : Display_Frame
Description : draw the above and below frame
Para :  NONE
Return:	 NONE 
*******************************************************************************/
void    Display_Frame(void)
{
   unsigned short  i, j;

   for (i = 0; i < 320; ++i)
   { // draw the below frame

      __Point_SCR(i, 0);
      for (j = 0; j < MIN_Y - 1; ++j)
         __Set_Pixel(LN2_COLOR);
   }
   for (i = 0; i < 320; ++i)
   { // draw the above frame

      __Point_SCR(i, MAX_Y + 3);
      for (j = MAX_Y + 3; j < 240; ++j)
         __Set_Pixel(LN2_COLOR);
   }
}
/*******************************************************************************
Function Name : Display_Grid
Description : draw the grid
Para :  NONE
Return:	 NONE 
*******************************************************************************/
void    Display_Grid(void)
{
   unsigned short  i, j;

   for (j = MIN_Y; j <= MAX_Y; j += 25)
   {
      for (i = MIN_X; i < MAX_X + 1; i += 5)
      {
         __Point_SCR(i, j);
         __Set_Pixel(GRD_COLOR); // draw the horizontal grid

      }
   }
   for (i = MIN_X; i <= MAX_X; i += 25)
   {
      for (j = MIN_Y; j <= MAX_Y; j += 5)
      {
         __Point_SCR(i, j);
         __Set_Pixel(GRD_COLOR); // draw the vertical grid

      }
   }
}
/*******************************************************************************
Function Name : Draw_CH1_SEG
Description : draw a vertical segment
Para : x is the coordinate, |y1-y2| is the segment heigth
Return:	 NONE 
*******************************************************************************/
void	Draw_CH1_SEG(unsigned short x, unsigned short y1, unsigned short y2)
{
   unsigned short  j, Tmp;

   Tmp = y2;
   if (y1 > y2)
   {
      y2 = y1;
      y1 = Tmp;
   }
   if (y2 >= MAX_Y)
      y2 = MAX_Y - 1;
   if (y1 <= MIN_Y)
      y1 = MIN_Y + 1;
   /*
    * switch ((y2-y1)) {                                //(the color defination�� case
    * 0:  Tmp =0xE780; break;  // 1110 0111 1000 0000 case 1:  Tmp =0xC680;
    * break;  // 1100 0110 1000 0000 case 2:  Tmp =0xA580; break;  // 1010
    * 0101 1000 0000 case 3:  Tmp =0x8480; break;  // 1000 0100 1000 0000
    * case 4:  Tmp =0x6380; break;  // 0110 0011 1000 0000 case 5:  Tmp
    * =0x4280; break;  // 0100 0010 1000 0000 default: Tmp =0x2180; break;
    * // 0010 0001 1000 0000 } 
    */
   Tmp = 0xE780;

   for (j = y1; j <= y2; ++j)
      __Add_Color(x + MIN_X, j, Tmp);
}
/****************************************************************************
Function Name : Draw_CH3_SEG
Description : draw a vertical segment
Para : x is the coordinate, |y1-y2| is the segment heigth
Return:	 NONE 
*****************************************************************************/
void     Draw_CH3_SEG(unsigned short x, unsigned short y1, unsigned short y2)
{
   unsigned short  j, Tmp;

   Tmp = y2;
   if (y1 > y2)
   {
      y2 = y1;
      y1 = Tmp;
   }
   if (y2 >= MAX_Y)
      y2 = MAX_Y - 1;
   if (y1 <= MIN_Y)
      y1 = MIN_Y + 1;
   /*
    * switch ((y2-y1)) {                                    //(the color defination��
    * case 0:  Tmp =0xF01C; break;  // 1111 0000 0001 1100 case 1:  Tmp
    * =0xD018; break;  // 1101 0000 0001 1000 case 2:  Tmp =0xB014; break;
    * // 1011 0000 0001 0100 case 3:  Tmp =0x9010; break;  // 1001 0000 0001
    * 0000 case 4:  Tmp =0x700C; break;  // 0111 0000 0000 1100 case 5:  Tmp
    * =0x5008; break;  // 0101 0000 0000 1000 default: Tmp =0x3004; break;
    * // 0011 0000 0000 0100 } 
    */
   Tmp = 0xF01C;
   for (j = y1; j <= y2; ++j)
      __Add_Color(x + MIN_X, j, Tmp);
}

/*******************************************************************************
Function Name : Erase_SEG
Description : use the Color to erase one segment
Para : i is the horizontal coordinate, |y1-y2| is the segment heigth
Return:	 NONE 
*******************************************************************************/
void	Erase_SEG(unsigned short i,unsigned short y1,unsigned short y2,unsigned short Color)
{
   unsigned short  j, Tmp;

   Tmp = y2;
   if (y1 > y2)
   {
      y2 = y1;
      y1 = Tmp;
   }
   if (y2 >= MAX_Y)
      y2 = MAX_Y - 1;
   if (y1 <= MIN_Y)
      y1 = MIN_Y + 1;
   for (j = y1; j <= y2; ++j)
      __Erase_Color(i + MIN_X, j, Color); //  erase the wave curve 

}
/*******************************************************************************
Function Name : Erase_Wave
Description : erase the wave curve
Para : NONE
Return:	 NONE 
*******************************************************************************/
void	Erase_Wave(void)
{
   unsigned short  i;
   unsigned char   y1, y2, y3, y4;

   y1 = View_Buffer[0];
   y3 = Ref_Buffer[0];

   for (i = 0; i < X_SIZE; ++i)
   {
      y2 = View_Buffer[i];
      y4 = Ref_Buffer[i];
      Erase_SEG(i, y1, y2, CH1_COLOR);
      Erase_SEG(i, y3, y4, CH3_COLOR);
      y1 = y2;
      y3 = y4;
   }
}
/*******************************************************************************
Function Name : Draw_Vn
Description : erase or compound the reference level,triggering level
Para :  NONE
Return:	 NONE 
*******************************************************************************/
void    Draw_Vn(unsigned short Vn, char Mode, unsigned short Color)
{
   unsigned short  i;

   for (i = MIN_X + 2; i < MAX_X; i += 2)
   {
      if (Mode == ADD)
         __Add_Color(i, Vn, Color);
      else
         __Erase_Color(i, Vn, Color);
   }
}
void            Draw_Vi(unsigned short Vi, char Mode, unsigned short Color)
{
   unsigned short  i;

   for (i = MIN_X + 5; i < MAX_X; i += 5)
   {
      if (Mode == ADD)
         __Add_Color(i, Vi, Color);
      else
         __Erase_Color(i, Vi, Color);
   }
}
void            Draw_Ti(unsigned short Ti, char Mode, unsigned short Color)
{
   unsigned short  j;

   for (j = MIN_Y + 3; j < MAX_Y; j += 3)
   {
      if (Mode == ADD)
         __Add_Color(Ti, j, Color);
      else
         __Erase_Color(Ti, j, Color);
   }
}
/*******************************************************************************
Function Name : Draw_Dot_Vn
Description : erase or compound the reference level,triggering level
Para :  Vn the Y position
Return:	 NONE 

*******************************************************************************/
void     Draw_Dot_Vn(unsigned short Vn, char Mode, unsigned short Color)
{
   if (Mode == ADD)
   {

      __Add_Color(MIN_X - 1, Vn - 2, Color);
      __Add_Color(MIN_X - 1, Vn - 1, Color);
      __Add_Color(MIN_X - 1, Vn, Color);
      __Add_Color(MIN_X - 1, Vn + 1, Color);
      __Add_Color(MIN_X - 1, Vn + 2, Color);

      __Add_Color(MIN_X, Vn - 1, Color);
      __Add_Color(MIN_X, Vn, Color);
      __Add_Color(MIN_X, Vn + 1, Color);

      __Add_Color(MIN_X + 1, Vn, Color);

      __Add_Color(MAX_X + 1, Vn - 2, Color);
      __Add_Color(MAX_X + 1, Vn - 1, Color);
      __Add_Color(MAX_X + 1, Vn, Color);
      __Add_Color(MAX_X + 1, Vn + 1, Color);
      __Add_Color(MAX_X + 1, Vn + 2, Color);

      __Add_Color(MAX_X, Vn - 1, Color);
      __Add_Color(MAX_X, Vn, Color);
      __Add_Color(MAX_X, Vn + 1, Color);

      __Add_Color(MAX_X - 1, Vn, Color);

   }
   else
   {

      __Erase_Color(MIN_X - 1, Vn - 2, Color); //{ this part will erase the left triangle
      __Erase_Color(MIN_X - 1, Vn - 1, Color);
      __Erase_Color(MIN_X - 1, Vn, Color);
      __Erase_Color(MIN_X - 1, Vn + 1, Color);
      __Erase_Color(MIN_X - 1, Vn + 2, Color);

      __Erase_Color(MIN_X, Vn - 1, Color);
      __Erase_Color(MIN_X, Vn, Color);
      __Erase_Color(MIN_X, Vn + 1, Color);

      __Erase_Color(MIN_X + 1, Vn, Color); //}

      __Erase_Color(MAX_X + 1, Vn - 2, Color); // { this part will erase the right triangle
      __Erase_Color(MAX_X + 1, Vn - 1, Color);
      __Erase_Color(MAX_X + 1, Vn, Color);
      __Erase_Color(MAX_X + 1, Vn + 1, Color);
      __Erase_Color(MAX_X + 1, Vn + 2, Color);

      __Erase_Color(MAX_X, Vn - 1, Color);
      __Erase_Color(MAX_X, Vn, Color);
      __Erase_Color(MAX_X, Vn + 1, Color);

      __Erase_Color(MAX_X - 1, Vn, Color); // }

      __Erase_Color(MIN_X - 1, Vn - 2, Color);
      __Erase_Color(MIN_X - 1, Vn - 1, Color);
      __Erase_Color(MIN_X - 1, Vn, Color);
      __Erase_Color(MIN_X - 1, Vn + 1, Color);
      __Erase_Color(MIN_X - 1, Vn + 2, Color);
   }
}
/***************************************************************************
Function Name : Draw_Dot_Ti
Description : erase or compound the triggering level
Para :  Vn the Y position
Return:	 NONE 
*****************************************************************************/

void       Draw_Dot_Ti(unsigned short Ti, char Mode, unsigned short Color)
{
   if (Mode == ADD)
   {

      __Add_Color(Ti - 2, MIN_Y - 1, Color);
      __Add_Color(Ti - 1, MIN_Y - 1, Color);
      __Add_Color(Ti, MIN_Y - 1, Color);
      __Add_Color(Ti + 1, MIN_Y - 1, Color);
      __Add_Color(Ti + 2, MIN_Y - 1, Color);

      __Add_Color(Ti - 1, MIN_Y, Color);
      __Add_Color(Ti, MIN_Y, Color);
      __Add_Color(Ti + 1, MIN_Y, Color);

      __Add_Color(Ti, MIN_Y + 1, Color);

      __Add_Color(Ti - 2, MAX_Y + 1, Color);
      __Add_Color(Ti - 1, MAX_Y + 1, Color);
      __Add_Color(Ti, MAX_Y + 1, Color);
      __Add_Color(Ti + 1, MAX_Y + 1, Color);
      __Add_Color(Ti + 2, MAX_Y + 1, Color);

      __Add_Color(Ti - 1, MAX_Y, Color);
      __Add_Color(Ti, MAX_Y, Color);
      __Add_Color(Ti + 1, MAX_Y, Color);

      __Add_Color(Ti, MAX_Y - 1, Color);

   } 
   else
   {

      __Erase_Color(Ti - 2, MIN_Y - 1, Color);
      __Erase_Color(Ti - 1, MIN_Y - 1, Color);
      __Erase_Color(Ti, MIN_Y - 1, Color);
      __Erase_Color(Ti + 1, MIN_Y - 1, Color);
      __Erase_Color(Ti + 2, MIN_Y - 1, Color);

      __Erase_Color(Ti - 1, MIN_Y, Color);
      __Erase_Color(Ti, MIN_Y, Color);
      __Erase_Color(Ti + 1, MIN_Y, Color);

      __Erase_Color(Ti, MIN_Y + 1, Color);

      __Erase_Color(Ti - 2, MAX_Y + 1, Color);
      __Erase_Color(Ti - 1, MAX_Y + 1, Color);
      __Erase_Color(Ti, MAX_Y + 1, Color);
      __Erase_Color(Ti + 1, MAX_Y + 1, Color);
      __Erase_Color(Ti + 2, MAX_Y + 1, Color);

      __Erase_Color(Ti - 1, MAX_Y, Color);
      __Erase_Color(Ti, MAX_Y, Color);
      __Erase_Color(Ti + 1, MAX_Y, Color);

      __Erase_Color(Ti, MAX_Y - 1, Color);

      __Erase_Color(Ti - 2, MIN_Y - 1, Color);
      __Erase_Color(Ti - 1, MIN_Y - 1, Color);
      __Erase_Color(Ti, MIN_Y - 1, Color);
      __Erase_Color(Ti + 1, MIN_Y - 1, Color);
      __Erase_Color(Ti + 2, MIN_Y - 1, Color);
   }
}
/*******************************************************************************
Function Name : Erase_View_Area
Description : erase one viewed area
Para :  NONE
Return:	 NONE 
*******************************************************************************/
void      Erase_View_Area(void)
{
   unsigned short  i;

   for (i = MIN_X + 2; i < MAX_X - 1; ++i)
   {
      __Erase_Color(i, MIN_Y + 3, LN2_COLOR);
      __Erase_Color(i, MIN_Y + 4, LN2_COLOR);
      __Erase_Color(i, MIN_Y + 2, CH2_COLOR);
      __Erase_Color(i, MIN_Y + 3, CH2_COLOR);
      __Erase_Color(i, MIN_Y + 4, CH2_COLOR);
      __Erase_Color(i, MIN_Y + 5, CH2_COLOR);
   }
}
/*******************************************************************************
Function Name : Draw_View_Area
Description : draw one area
Para :  NONE
Return:	 NONE 
*******************************************************************************/
void      Draw_View_Area(void)
{
   unsigned short  i, j;

   for (i = MIN_X + 2; i < MAX_X - 1; ++i)
   {
      __Add_Color(i, MIN_Y + 3, LN2_COLOR);;
      __Add_Color(i, MIN_Y + 4, LN2_COLOR);
   }
   j = MIN_X + ((((Item_Index[X_POSITION] + 150 - 4096)) * X_SIZE) >> 12);

   for (i = MIN_X + 2; i < MAX_X - 1; ++i)
   {
      if ((i >= j) && (i <= j + 22))
      {
         __Add_Color(i, MIN_Y + 2, CH2_COLOR);
         __Add_Color(i, MIN_Y + 3, CH2_COLOR);
         __Add_Color(i, MIN_Y + 4, CH2_COLOR);
         __Add_Color(i, MIN_Y + 5, CH2_COLOR);
      } 
	  else
      {
         __Erase_Color(i, MIN_Y + 2, CH2_COLOR);
         __Erase_Color(i, MIN_Y + 3, CH2_COLOR);
         __Erase_Color(i, MIN_Y + 4, CH2_COLOR);
         __Erase_Color(i, MIN_Y + 5, CH2_COLOR);
      }
   }
}
/*******************************************************************************
Function Name : Display_Str
Description : print one string in the specific  position
Para : (x0,y0) is the coordinate of the start point of the string
       Color is the string color
	   Mode=PRN Normal replace Display, Mode=INV Inverse replace Display
	   s is the string
Return:	 NONE 
*******************************************************************************/
void      Display_Str(short x0, short y0, short Color, char Mode, unsigned const char *s)//320*240
{
   short     i, j, k, b;

   LCD_SET_WINDOW(x0, LCD_X2, y0, y0 + 13);//the whole area that the string will be put, 13 is the heighth of the string
   for (j = 0; j < 14; ++j)
   {
      if (Mode == 0) //PRN
         __Set_Pixel(BLACK); // Normal replace Display

      if (Mode == 1) //INV
         __Set_Pixel(Color); // Inverse replace Display

   }
   while (*s != 0)
   {
      b = 0x0000;
      if (*s == 0x21)// 0x21 is '!'
         k = 4;
      else
         k = 8; //the '!' will take up 4 pixels,others will take up 8 pixels
      for (i = 0; i < k; ++i)
      {
         if ((*s > 0x21))//before the '!' is just the control character
            b = __Get_TAB_8x14(*s, i);//one character will take up 8*14 pixels,get the font from one table
         for (j = 0; j < 14; ++j) //means the char height pixel
         {
            if (b & 4)
            {
               if (Mode == 0)
                  __Set_Pixel(Color);
               if (Mode == 1)
                  __Set_Pixel(BLACK);
            } 
			else
            {
               if (Mode == 0)
                  __Set_Pixel(BLACK);
               if (Mode == 1)
                  __Set_Pixel(Color);
            }
            b >>= 1;
         }
      }
      ++s; // the pointer of the string will add one

   }
   LCD_SET_WINDOW(LCD_X1, LCD_X2, LCD_Y1, LCD_Y2); // Restore full screen

}
/******************************** END OF FILE *********************************/
